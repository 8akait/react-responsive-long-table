import React, {Component} from 'react';
import './DataTable.screen.css';
import { FixedSizeList as List } from 'react-window';
import * as dataLimitConfig from '../../configs/dataLimit.config.json';

import LazyLoad from '../../utils/LazyLoad';


const ROW_HEIGHT = 60;
const PAGE_LIMIT = dataLimitConfig.totalRowsPerPage;
const LAZY_LOAD_CONTENT_LIMIT = dataLimitConfig.lazyLoadRowsPerPage;

export interface HeaderDataType {
    id: string,
    label: string,
    numeric: boolean,
    width?: string,
}
export interface ListDataType {
    id: string
    product: string | number
    price: string | number
    [k: string]: any
}
export interface RowClickedDataType {
    rowIndex: number
    rowData: ListDataType
}

export interface State {
    screenWidth: any
    pageNumber: number
    totalRowsPerPage: number
}

interface Props {
    listData: ListDataType[]| undefined;
    headerData: HeaderDataType[];
    onRowClick: ({rowIndex, rowData}: {rowIndex: number, rowData: ListDataType}) => void;
    onSelectionChange: (x: string | 'All') => void;
    selectedIds: string[] | undefined;
    imageDatas: string[]
}

class DataTableScreen extends Component<Props, State> {
    private titleToWidthMap: {[x: string]: {width: string, isRightAlign: boolean}} = {};
    private totalPages: number;
    constructor(props: Props){
        super(props);

        this.state = {
            screenWidth: window.innerWidth,
            pageNumber: 0,
            totalRowsPerPage: this.getTotalRowsPerPage(0)
        }
        this.props.headerData.map((data: HeaderDataType) => {
            const {id, width, numeric} = data;
            this.titleToWidthMap[id] = {
                width: width ? width: '100px',
                isRightAlign: numeric ? numeric : false
            };
            return undefined;
        })

        const listData: ListDataType[] = this.props.listData || [];
        const length = listData.length;
        this.totalPages = Math.ceil(length / PAGE_LIMIT);

        this.renderListData = this.renderListData.bind(this);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }
    componentDidMount() {
        window.addEventListener('resize', this.updateWindowDimensions);
      }
      
    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }
      
    updateWindowDimensions() {
        this.setState({ screenWidth: window.innerWidth });
    }
    getTotalRowsPerPage(pageNumber: number) {
        const upperLimit = (pageNumber + 1) * PAGE_LIMIT;
        const lowerLimit = pageNumber * PAGE_LIMIT;
        const listData: ListDataType[] = this.props.listData || [];
        const length = listData.length;
        return upperLimit < length ? PAGE_LIMIT: (length - lowerLimit);
    }
    renderHeader() {
        const {headerData, onSelectionChange, listData} = this.props;
        return (
            <div className="row headerContainer" style={{height: ROW_HEIGHT}}>
                <input type="checkbox" 
                        checked={(this.props.selectedIds && listData && this.props.selectedIds.length === listData.length) 
                                    ? true : false} 
                        onChange={() => {onSelectionChange('All')}} 
                    />

                {headerData.map((data: HeaderDataType) => {
                    const {id, label, width} = data;
                    return (<div key={id} className="title" style={{width: width? width : this.titleToWidthMap[id].width}}>
                                {label}
                            </div>);
                })}
            </div>
        )
    }
    onClickPageNumber(pageNumber: number) {
        this.setState({
            pageNumber: pageNumber,
            totalRowsPerPage: this.getTotalRowsPerPage(pageNumber)
        })
    }
    renderPageNumberPanel(totalPages: number) {
        const numbers:number[] = []
        for(let i=0; i < totalPages; i++){
            numbers.push(i)
        }
        console.log(numbers)
        return (
            <div className="pagePanel">
                {numbers.map((num: number) => {
                    return (
                        <div key={'pageNum'+num} className="pageNumberStyle" onClick={() => this.onClickPageNumber(num)}>
                            {num + 1}
                        </div>  
                    )
                })}
            </div>
        )
    }
    renderListData({index}: any) {
        const {listData, onRowClick, onSelectionChange, selectedIds, imageDatas} = this.props;
        const upperLimit = (this.state.pageNumber + 1) * PAGE_LIMIT;
        const lowerLimit = this.state.pageNumber * PAGE_LIMIT;
        const selectedListData = listData ? listData.slice(lowerLimit, upperLimit): undefined;
        const imageIndexNumber = this.state.pageNumber * PAGE_LIMIT + index;

        if(selectedListData && selectedListData[index]) {
            const data = selectedListData[index];
            const keys = Object.keys(data);
            const {id} = data;
            return (
                <div className={index % 2 ? 'ListItemOdd row' : 'ListItemEven row'} 
                    onClick={() => onRowClick({rowIndex: index, rowData: data})}
                    style={{height: ROW_HEIGHT}}
                    key={index}
                    >

                    <input type="checkbox" 
                        checked={selectedIds && selectedIds.includes(id) ? true : false} 
                        onChange={() => {onSelectionChange(id)}} 
                    />
                    {keys.map((key: string | number) => {
                        if(key === 'id') return undefined;
                        const {width, isRightAlign} = this.titleToWidthMap[key]
                        return (
                            <div key={index+ key} className="title" style={{width, textAlign: isRightAlign ? 'right': 'center'}}>
                                {data[key]}
                            </div>
                        )
                    })}
                    {
                        imageDatas[imageIndexNumber] ? 
                        <img src={imageDatas[imageIndexNumber]} alt="" className="thumbnail"/> :
                        undefined
                    }
                </div>
            )
        }
        return <div> No Data</div>;
    } 
    renderListDataLazyLoad(data:any, lazyIndex: number, totalRowsInLazyIndex: number) {
        const {index}= data
        const {listData, onRowClick, onSelectionChange, selectedIds, imageDatas} = this.props;

        const upperLimit = (this.state.pageNumber + 1) * PAGE_LIMIT+ lazyIndex * totalRowsInLazyIndex;
        const lowerLimit = this.state.pageNumber * PAGE_LIMIT + lazyIndex * totalRowsInLazyIndex;

        const selectedListData = listData ? listData.slice(lowerLimit, upperLimit): undefined;
        const imageIndexNumber = this.state.pageNumber * PAGE_LIMIT + index;

        if(selectedListData && selectedListData[index]) {
            const data = selectedListData[index];
            const keys = Object.keys(data);
            const {id} = data;
            return (
                <div className={index % 2 ? 'ListItemOdd row' : 'ListItemEven row'} 
                    onClick={() => onRowClick({rowIndex: index, rowData: data})}
                    style={{height: ROW_HEIGHT}}
                    key={imageIndexNumber}
                    >

                    <input type="checkbox" 
                        checked={selectedIds && selectedIds.includes(id) ? true : false} 
                        onChange={() => {onSelectionChange(id)}} 
                    />
                    {keys.map((key: string | number) => {
                        if(key === 'id') return undefined;
                        const {width, isRightAlign} = this.titleToWidthMap[key]
                        return (
                            <div key={imageIndexNumber+ key} className="title" style={{width, textAlign: isRightAlign ? 'right': 'center'}}>
                                {data[key]}
                            </div>
                        )
                    })}
                    {
                        imageDatas[imageIndexNumber] ? 
                        <img src={imageDatas[imageIndexNumber]} alt="" className="thumbnail"/> :
                        undefined
                    }
                </div>
            )
        }
        return <div> No Data</div>;
    } 
    renderLazyLoad(totalRowsPerPage: number) {
        const output: any = [];
        const totalLazyLoadContainers = Math.ceil(totalRowsPerPage / LAZY_LOAD_CONTENT_LIMIT);
        const totalRowsPerLayLoadCOntainer = LAZY_LOAD_CONTENT_LIMIT < totalRowsPerPage ? LAZY_LOAD_CONTENT_LIMIT : totalRowsPerPage;
        const lazyLoadContainerHeight =  totalRowsPerLayLoadCOntainer * ROW_HEIGHT;

        const lazyLoadIdPerfix = 'lazyContainer'

        output.push (
                <div key={`${lazyLoadIdPerfix}0`} id={`${lazyLoadIdPerfix}0`} style={{height:lazyLoadContainerHeight, marginTop: ROW_HEIGHT}}>
                    {
                        (() => {
                            const result = []
                            for(let z=0; z < totalRowsPerLayLoadCOntainer; z++) {
                                result.push(this.renderListDataLazyLoad({index: z}, 0, totalRowsPerLayLoadCOntainer))
                            }
                            return result;
                        })()
                    }
                 </div>
        )      
        for(let i=1; i<totalLazyLoadContainers; i++){
            output.push (
                <LazyLoad key={`${lazyLoadIdPerfix}${i}`} ancesterId={`${lazyLoadIdPerfix}${i-1}`} >
                    <div id={`${lazyLoadIdPerfix}${i}`} style={{height:lazyLoadContainerHeight}}>
                        {
                            (() => {
                                const result = []
                                for(let z=0; z < totalRowsPerLayLoadCOntainer; z++) {  
                                    result.push(
                                        this.renderListDataLazyLoad({index: z}, i, totalRowsPerLayLoadCOntainer)
                                    )
                                }
                                return result;
                            })()
                        }
                     </div>
                </LazyLoad>
            )
        }
        return output;
    }
    render() {
        const {totalRowsPerPage} = this.state;
        const height =  totalRowsPerPage * ROW_HEIGHT;
        return(
            <div className="DataTableContainer">
                {this.renderHeader()}
                {
                    dataLimitConfig.isLazyLoadingActive ? this.renderLazyLoad(totalRowsPerPage)
                    :
                        (<List
                        width={this.state.screenWidth * 0.9}
                        height={height}
                        itemCount={totalRowsPerPage}
                        itemSize={ROW_HEIGHT}
                        style={{marginTop: '50px', border: '1px solid orange'}}
                        >
                            {this.renderListData}
                    </List>)
                }
                {this.renderPageNumberPanel(this.totalPages)}
            </div>

        )
    }
}

export default DataTableScreen;