import React, { Component } from 'react';

import DataTableScreen, {ListDataType, HeaderDataType, RowClickedDataType} from './components/dataTable/DataTable.screen';
import Loader from 'react-loader-spinner';

import './App.css';
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import { LoremIpsum } from "lorem-ipsum";

import axios from 'axios';

import * as dataLimitConfig from './configs/dataLimit.config.json';

const TOTAL_DATA_COUNT = dataLimitConfig.totalRows;

const hData: HeaderDataType[] = [
  {
  'id': 'product', // Uniq ID to identify column
  'label': 'Product',
  'numeric': false,
  'width': '150px'
  }, 
  {
  'id': 'price',
  'label': 'Price',
  'numeric': true, // Right Align
  }]

const LData: ListDataType[] = [
  {
  'id': 'some_id1',
  'product': 'product1', // Key is column id and value is
  'price': 15.2
  }, {
  'id': 'some_id2',
  'product': 'product2',
  'price': '$15.5'
  }
];

interface State {
  listData: ListDataType[];
  headerData: HeaderDataType[] | undefined;
  selectedIds: string[];
  imageData: string[];
}
interface Props {}

class App extends Component<Props, State> {
  private tempData: ListDataType[] = [];
  constructor(props: Props){
    super(props);
    this.generateData(TOTAL_DATA_COUNT);

    this.state = {
      listData: (process.env.NODE_ENV === 'test') ? LData : this.tempData,
      headerData: hData,
      selectedIds: [],
      imageData: []
    }

    this.onRowClick = this.onRowClick.bind(this);
    this.onSelectionChange = this.onSelectionChange.bind(this);
  }
  generateData(count: number){
    const temp: ListDataType = {id: '', product: '', price: ''}
    this.tempData = Array(count).fill(temp).map((val: any, idx: number) => {
      const lorem = new LoremIpsum({
        sentencesPerParagraph: {
          max: 8,
          min: 4
        },
        wordsPerSentence: {
          max: 16,
          min: 4
        }
      })
      return {
        id: ''+ idx, 
        product: lorem.generateWords(1),
        price: idx
      }
    });
  }
  async componentDidMount() {
    try {
      const response: any = await axios.get('https://jsonplaceholder.typicode.com/photos');
      const {data} = response;
      const images: string[] = []
      data.map(({thumbnailUrl}:{thumbnailUrl: string}) => images.push(thumbnailUrl))
      this.setState({imageData: images})
    }catch(error){
      console.log(error);
    }
  }
  onRowClick({rowIndex, rowData}: RowClickedDataType) {
    console.log(`Index : ${rowIndex} :> ${rowData}`);
  }
  onSelectionChange(id: string | 'All'){
    const allIds = this.state.selectedIds;
    const {listData} = this.state;
    if(id === 'All') {
      this.setState((prevState: State) => {
        if(allIds && allIds.length === listData.length){
          return {
            selectedIds: []
          }
        }
        return {
          selectedIds: listData.map(({id}: {id: string}) => id)
        }
      }, () => {
        console.log('Selected Ids: ' + allIds)
      })
    } else {
      const clone: string[] = [...allIds];
      if (allIds && allIds.includes(id)) {
        this.setState((prevState: State) => {
          return {
            selectedIds: clone.filter((data: string) => data !== id)
          }
        });
      } else {
        clone.push(id);
        this.setState((prevState: State) => {
          return {
            selectedIds: clone
          }
        });
      }
      console.log(id)
    }
  }
  render() {
    const {listData, headerData} = this.state;
    return (
      <div className="App">
        { headerData ? 
            <DataTableScreen 
              listData={listData}
              headerData={headerData} 
              onRowClick={this.onRowClick}
              onSelectionChange={this.onSelectionChange}
              selectedIds={this.state.selectedIds}
              imageDatas={this.state.imageData}
              />
              : <Loader
                  type="Puff"
                  color="#00BFFF"
                  height={100}
                  width={100}
                />
        }
      </div>
    );
  }
}

export default App;
