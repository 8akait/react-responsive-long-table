import React, { PureComponent } from 'react';

export interface Props{
    ancesterId: any
}
interface State {
    intersecting: boolean
}

export default class LazyLoad extends PureComponent<Props, State> {
    private observer: any;
  constructor(props: Props) {
    super(props);
    this.state = {
      intersecting: false
    };
  }
  componentWillMount() {
    const options = {
      root: document.querySelector(`#${this.props.ancesterId}`),
      rootMargin: '200px',
      threshold: 0.0
    };
    this.observer = new IntersectionObserver(
      this.handleIntersect.bind(this),
      options
    );
  }
  componentDidMount() {
    const element = document.getElementById(this.props.ancesterId);
    this.observer.observe(element);
  }
  handleIntersect(entries: any, observer: any) {
    entries.forEach((entry: any) => {
      const { isIntersecting, intersectionRatio } = entry;
      if (
        // this.state.intersecting === false &&
        (isIntersecting === true || intersectionRatio) > 0
      ) {
        this.setState({ intersecting: true });
        this.observer.disconnect();
        this.observer = null;
      }
    });
  }
  componentWillUnmount() {
    if (this.observer) {
      this.observer.disconnect();
      this.observer = null;
    }
  }
  render() {
    return this.state.intersecting ? (
      this.props.children
    ) : (
      <div id={this.props.ancesterId} style={{ height: '100%' }} />
    );
  }
}
