# React responsive large table or list

React components for efficiently rendering large lists or tabular data in lazy mode or in normal mode by pagination.

## Getting Started

There is a config file named "dataLimit.config.json" in config folder where you can set number of data that you want to test. <br />

`totalRows`: total number of data rows <br />
`totalRowsPerPage`: total rows you want to display per page <br />
`lazyLoadRowsPerPage`: total number of rows to be loaded on demand <br />
`isLazyLoadingActive` whether to load table in lazy mode or normal mode <br />

## Installing

#### `yarn install`

## Available Scripts

In the project directory, run:

#### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### `yarn build`

Builds the app for production to the `build` folder.<br>
Then run : `serve`

## Notes

- Use Server-side pagination to load the data in chunks. This will reduce the network load if you are fetching large amount of data
- Use lazy loading mode for better "above the fold" page load time.
- use pagination to decrease in-use js heap memory.
